
package psdtocssshadowconverter;


import java.util.Scanner;

public class PsdToCssShadowConverter {

    public CssShadow convert(PsdShadow psd) {
        
        float angle = (180 - psd.getAngle()) * (float) Math.PI / 180;
        String color = "rgba(" + psd.getColor() + "," + psd.getOpacity() / 100 + ")";
        
        float offSetX = Math.round(Math.cos(angle) * psd.getDistance());
        float offSetY = Math.round(Math.sin(angle) * psd.getDistance());
        float spreadRadius = psd.getSize() * psd.getSpread() / 100;
        float blurRadius = psd.getSize() - spreadRadius;
        
        return new CssShadow(color, offSetX, offSetY, blurRadius, spreadRadius);
    }
    
    public static void main(String[] args) {
        
        String color;
        float opacity, angle, distance, spread, size;
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the RGB color from PSD");
        color = sc.nextLine();
        System.out.println("Enter the opacity from PSD");
        opacity = sc.nextFloat();
        System.out.println("Enter the angle from PSD");
        angle = sc.nextFloat();
        System.out.println("Enter the distance from PSD");
        distance = sc.nextFloat();
        System.out.println("Enter the spread from PSD");
        spread = sc.nextFloat();
        System.out.println("Enter the size from PSD");
        size = sc.nextFloat();
        
        System.out.println();
        System.out.println("Css format: ");
        System.out.println(new PsdToCssShadowConverter().convert(new PsdShadow(color, opacity, angle, distance, spread, size)).toString());
    }
    
}
