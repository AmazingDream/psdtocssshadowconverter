
package psdtocssshadowconverter;

public class CssShadow {
    
    private String color;
    private float offSetX;
    private float offSetY;
    private float blurRadius;
    private float spreadRadius;
    
    public CssShadow(String color, float offSetX, float offSetY, 
            float blurRadius, float spreadRadius) {
        
        this.color = color;
        this.offSetX = offSetX;
        this.offSetY = offSetY;
        this.blurRadius = blurRadius;
        this.spreadRadius = spreadRadius;
    }
    
    @Override
    public String toString() {
        return "Color: " + color + "\n" + "OffSetX: " + offSetX + "\n"+
                "OffSetY: " + offSetY + "\n" +  "BlurRadius: " + blurRadius + "\n"
                + "SpreadRadius: " + spreadRadius + "\n";
    }
    
    public String getColor() {
        return color;
    }
    
    public float getOffSetX() {
        return offSetX;
    }
    
    public float getOffSetY() {
        return offSetY;
    }
    
    public float getBlurRadius() {
        return blurRadius;
    }
    
    public float getSpreadRadius() {
        return spreadRadius;
    }
}
