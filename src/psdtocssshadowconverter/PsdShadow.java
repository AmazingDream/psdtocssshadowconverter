
package psdtocssshadowconverter;

public class PsdShadow {
    
    private String color; 
    private float opacity;
    private float angle;
    private float distance;
    private float spread;
    private float size;
    
    public PsdShadow(String color, float opacity, float angle, float distance,
            float spread, float size) {
        
        this.color = color;
        this.opacity = opacity;
        this.angle = angle;
        this.distance = distance;
        this.spread = spread;
        this.size = size;
    }
    
    @Override
    public String toString() {
        return  "Color: " + color + "\n" + "Opacity: " + opacity + "\n" +
                "Angle: " + angle  + "\n" + "Distance: " + distance + "\n" +
                "Spread: " + spread + "\n" + "Size: " + size + "\n";
    }
    
    public String getColor() {
        return color;
    }
    
    public float getOpacity() {
        return opacity;
    }
    
    public float getAngle() {
        return angle;
    }
    
    public float getDistance() {
        return distance;
    }
    
    public float getSpread() {
        return spread;
    }
    
    public float getSize() {
        return size;
    }
}